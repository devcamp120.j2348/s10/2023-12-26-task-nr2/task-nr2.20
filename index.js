const express = require("express");
const app = express();
const port = 8000;
const drinkRouter = require("./app/routes/drinkRouter")
const voucherRouter = require("./app/routes/voucherRouter")
const userRouter = require("./app/routes/userRouter")
const orderRouter = require("./app/routes/orderRouter")
const mongoose = require("mongoose");
      mongoose.connect("mongodb://127.0.0.1:27017/CRUD_pizza365")
              .then(() => console.log("Successful"))
              .catch((err) => console.log(err))



app.use(express.json());

app.use((req, res, next) => {
  console.log("Current date: ", new Date());
  next()
})
app.use((req, res, next) => {
  console.log("Request method: ", req.method);
  next()
})

app.use("/api/", drinkRouter)
app.use("/api/", voucherRouter)
app.use("/api/", userRouter)
app.use("/api/", orderRouter)
app.listen(port, () => {
  console.log("app listening on", port)
})