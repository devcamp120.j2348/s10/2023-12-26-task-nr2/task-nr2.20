const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const orderSchema = new Schema({
  _id: mongoose.Types.ObjectId,
 orderCode: {
  type: String,
  unique: true,
  default: function makeid() {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 5; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

 },
 pizzaSize: {
  type: String,
  required: true,
 },
 pizzaType: {
  type: String,
  required: true
 },
voucher:
  {
    type: mongoose.Types.ObjectId,
    type: String,
    ref: "voucher"
  }
,
 drink:
  {
    type: mongoose.Types.ObjectId,
    type: String,
    ref: "drink"
  }
,
 status: {
  type: String,
  required: true
 }
},{ timestamps: true })

module.exports = mongoose.model("order", orderSchema)