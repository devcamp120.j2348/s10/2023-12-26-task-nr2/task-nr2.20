const mongoose = require("mongoose");
const userModel = require("../model/userModel");

const createUser = (req, res) => {
  const {
    fullName,
    email,
    address,
    phone,
    orders
  } = req.body

  if(fullName == ""){
    return res.status(400).json({
      message: "Full name need to filled"
    })
  }
  if(email == ""){
    return res.status(400).json({
      message: "Email need to filled"
    })
  }
  if(address == ""){
    return res.status(400).json({
      message: "Address need to filled"
    })
  }
  if(phone == ""){
    return res.status(400).json({
      message: "Phone need to filled"
    })
  }
   var newUser = new userModel({
    _id: new mongoose.Types.ObjectId,
    fullName,
    email,
    address,
    phone,
    orders
   })
   console.log(newUser)
   userModel.create(newUser)
            .then((data) => {
              res.status(201).json({
                message: "Create successfully",
                user: data
              })
            })
            .catch((error) => {
              console.error(error);
              res.status(500).json({
                message: "Error"
              })
            })
}

const getAllUser = async (req, res) => {
  try {
    const limit = req.query.limit;
    const skip = req.query.skip;
    const sort = req.query.skip;
    let result = null;
    let query = userModel.find();

    if (limit) {
       query = query.limit(limit);
    }

    if (skip) {
      query = query.skip(skip);
    }

    query.sort('-fullName');
  
    result = await query;

    return res.status(200).json({
      message: "Get user successfully",
      users: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const getAllUsersWithSort = async (req, res) => {
  try {

    let query = userModel.find();
    query = query.sort('-fullName');
    let result = await query;

    return res.status(200).json({
      message: "Get user successfully",
      users: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const getUserById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }

  try {
    const result = await userModel.findById(id);
    if (result) {
      return res.status(200).json({
        message: "Get user successfully",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "No user id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const updateUserById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  const {
    fullName,
    email,
    address,
    phone,
    orders
  } = req.body
  if(fullName == ""){
    return res.status(400).json({
      message: "Full name need to filled"
    })
  }
  if(email == ""){
    return res.status(400).json({
      message: "Email need to filled"
    })
  }
  if(address == ""){
    return res.status(400).json({
      message: "Address need to filled"
    })
  }
  if(phone == ""){
    return res.status(400).json({
      message: "Phone need to filled"
    })
  }

  try {
    var newUserUpdate = {}
    if(fullName){
      newUserUpdate.fullName = fullName
    }
    if(email){
      newUserUpdate.email = email
    }
    if(address){
      newUserUpdate.address = address
    }
    if(phone){
      newUserUpdate.phone = phone
    }
    
    const result = await userModel.findByIdAndUpdate(id, newUserUpdate)
    if(result){
      return res.status(201).json({
        message:"Update User successful",
        review: result
      })
    }else {
      return res.status(400).json({
        message: "User Id can not found"
      })
    }
    
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
const deleteUserById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }

  try {
    const result = await userModel.findByIdAndDelete(id);
    if (result) {
      return res.status(200).json({
        message: "Delete User successfully"
      })
    } else {
      return res.status(404).json({
        message: "No User id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
module.exports = {
  createUser,
  getAllUser,
  getUserById,
  deleteUserById,
  updateUserById,
  getAllUsersWithSort
}