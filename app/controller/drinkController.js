const drinkModel = require("../model/drinkModel")
const orderModel = require("../model/orderModel")
const mongoose = require("mongoose")

const createDrink = (req, res) => {
  const {
    orderId,
    reqCode,
    reqName,
    reqPrice
  } = req.body

  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      message: "Order Id is not valid"
    })
  }

  if (reqCode == "" || reqName == "" || reqPrice == "") {
    return res.status(400).json({
      message: "All information need to be filled"
    })
  }
  var newDrink = new drinkModel({
    _id: new mongoose.Types.ObjectId,
    maNuocUong: reqCode,
    tenNuocUong: reqName,
    donGia: reqPrice
  })
  drinkModel.create(newDrink)
    .then((data) => {
      orderModel.findByIdAndUpdate(orderId, {
         drink: data._id 
      })
        .then((success) => {
          return res.status(201).json({
            message: "Create drink successful",
            review: data
          })
        })
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Error" + error.message
      })
    })
}

const getAllDrink = async (req, res) => {
  try {
    const result = await drinkModel.find();

    return res.status(200).json({
      message: "Get drink successfully",
      data: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const getDrinkById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await drinkModel.findById(id);
    if (result) {
      return res.status(200).json({
        message: "Get drink successfully",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "No drink id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const updateDrinkById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  const {
    reqCode,
    reqName,
    reqPrice
  } = req.body

  if (reqCode == "") {
    return res.status(400).json({
      message: "Code not valid"
    })
  }

  if (reqName == "") {
    return res.status(400).json({
      message: "Name not valid"
    })
  }

  if (reqPrice == "") {
    return res.status(400).json({
      message: "Price not valid"
    })
  }
  try {
    var newDrink = {};
    if (reqCode) {
      newDrink.maNuocUong = reqCode
    }
    if (reqName) {
      newDrink.tenNuocUong = reqName
    }
    if (reqPrice) {
      newDrink.donGia = reqPrice
    }


    const result = await drinkModel.findByIdAndUpdate(id, newDrink);
    if (result) {
      return res.status(200).json({
        message: "Update drink successfully",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "No drink id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
const deleteDrinkById = async (req, res) => {
  const id = req.params.id
  const orderId = req.params.orderId
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }

  try {
    const result = await drinkModel.findByIdAndDelete(id);

    if(orderId != undefined){
      await orderModel.findByIdAndUpdate(orderId, {
        drink: ""
      })
    } 
    if (result) {
      return res.status(200).json({
        message: "Delete drink successfully"
      })
    } else {
      return res.status(404).json({
        message: "No order id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}


module.exports = {
  getAllDrink,
  createDrink,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById
}