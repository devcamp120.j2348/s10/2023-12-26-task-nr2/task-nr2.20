const orderModel = require("../model/orderModel");
const userModel = require("../model/userModel")

const mongoose = require("mongoose");

const createOrder = (req, res) => {
  const {
    userId,
    orderCode,
    pizzaSize,
    pizzaType,
    voucher,
    drink,
    status
  } = req.body

  if(!mongoose.Types.ObjectId.isValid(userId)){
    return res.status(400).json({
      message: "Course Id is not valid"
    })
  }
  if(pizzaSize == ""){
    res.status(400).json({
      message: "Size cant be blank"
    })
  }
  if(pizzaType == ""){
    res.status(400).json({
      message: "Type cant be blank"
    })
  }
  if(status == ""){
    res.status(400).json({
      message: "Status cant be blank"
    })
  }
 var newOrder = {
    _id: new mongoose.Types.ObjectId,
    orderCode,
    pizzaSize,
    pizzaType,
    voucher,
    drink,
    status
 }
 orderModel.create(newOrder)
           .then((data) => {
            userModel.findByIdAndUpdate(userId, {
              $push: { orders: data._id }
           })
           .then((success) => {
            return res.status(201).json({
              message: "Create review successful",
              order: data
            })
          })
          })
          .catch((err) => {
            return res.status(500).json({
              message: "Error"
            })
          })
  
}

const getAllOrder = async (req, res) => {
  try {
    const result = await orderModel.find();
    return res.status(200).json({
      message: "Get order successfully",
      orders: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const getOrderById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }

  try {
    const result = await orderModel.findById(id);
    if (result) {
      return res.status(200).json({
        message: "Get Order successfully",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "No order id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
const updateOrderById = async (req, res) => {
  const id = req.params.id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  const {
    status
  } = req.body
  if(status == ""){
    return res.status(400).json({
      message: "status need to filled"
    })
  }

  try {
    var newOrderUpdate = {}
    if(status){
      newOrderUpdate.status = status
    }
    const result = await orderModel.findByIdAndUpdate(id, newOrderUpdate)
    if(result){
      return res.status(201).json({
        message:"Update order successful",
        order: result
      })
    }else {
      return res.status(400).json({
        message: "Order Id can not found"
      })
    }
    
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const deleteOrderById = async (req, res) => {
  const id = req.params.id
  const userId = req.params.userId
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await orderModel.findByIdAndDelete(id);

    if(userId != undefined){
      await userModel.findByIdAndUpdate(userId, {
        $pull: { orders: id }
      })
    } 
    if (result) {
      return res.status(200).json({
        message: "Delete order successfully"
      })
    } else {
      return res.status(404).json({
        message: "No order id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
module.exports = {
  createOrder,
  getAllOrder,
  getOrderById,
  updateOrderById,
  deleteOrderById
}