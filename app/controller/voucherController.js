const voucherModel = require("../model/voucherModel")
const orderModel = require("../model/orderModel")
const mongoose = require("mongoose");

const createVoucher = (req, res) => {
  const {
    orderId,
    reqCode,
    reqDiscount,
    reqNote
  } = req.body
console.log(req.body)
if (!mongoose.Types.ObjectId.isValid(orderId)) {
  return res.status(400).json({
    message: "Order Id is not valid"
  })
}

  if(reqCode == ""){
    return res.status(400).json({
      message: "Code cant be blank"
    })
  }
  if(reqDiscount == ""){
    return res.status(400).json({
      message: "Discount cant be blank"
    })
  }

  var newVoucher = new voucherModel({
    _id: new mongoose.Types.ObjectId,
    maVoucher: reqCode,
    phanTramGiamGia: reqDiscount,
    ghiChu: reqNote
  })
  voucherModel.create(newVoucher)
  .then((data) => {
    orderModel.findByIdAndUpdate(orderId, {
       voucher: data._id 
    })
      .then((success) => {
        return res.status(201).json({
          message: "Create voucher successful",
          voucher: data
        })
      })
  })
              .catch((err) => {
                return res.status(500).json({
                  message:"Error" + err.message
                })
              })
}

const getAllVoucher = async (req, res) => {
  try {
    const result = await voucherModel.find();
    return res.status(200).json({
      message:"Get all voucher",
      voucher: result
    })
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const getVoucherById = async (req, res) => {
  const id = req.params.id;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await voucherModel.findById(id);
    if(result){
      return res.status(200).json({
        message: "Get voucher by Id",
        drink: result
      })
    } else {
      return res.status(400).json({
        message: "Can not find voucher id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}

const updateVoucherById = async (req, res) => {
  const id = req.params.id;
  const {
    reqCode,
    reqDiscount,
    reqNote
  } = req.body

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }

  if (reqCode == "") {
    return res.status(400).json({
      message: "Code not valid"
    })
  }

  if (reqDiscount == "") {
    return res.status(400).json({
      message: "Discount not valid"
    })
  }
try {
  const newVoucher = {};
  if(reqCode){
    newVoucher.maVoucher = reqCode;
  }
  if(reqDiscount){
    newVoucher.phanTramGiamGia = reqDiscount
  }
  if(reqNote){
    newVoucher.ghiChu = reqNote
  }
  const result = await voucherModel.findByIdAndUpdate(id, newVoucher)
  if(result){
    return res.status(200).json({
      message: "Update successfully",
      voucher: result
    })
  } else {
    return res.status(400).json({
      message: "Cant find voucher ID"
    })
  }
} catch (error) {
  return res.status(500).json({
    message:"Error" + error.message
  })
}

}

const deleteVoucher = async (req, res) => {
  const id = req.params.id;
  const orderId = req.params.orderId
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      message: "Id is not valid"
    })
  }
  try {
    const result = await voucherModel.findByIdAndDelete(id);

    if(orderId != undefined){
      await orderModel.findByIdAndUpdate(orderId, {
        voucher: ""
      })
    } 
    if (result) {
      return res.status(200).json({
        message: "Delete drink successfully"
      })
    } else {
      return res.status(404).json({
        message: "No order id founded"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Error"
    })
  }
}
module.exports = {
  createVoucher,
  getAllVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucher
}