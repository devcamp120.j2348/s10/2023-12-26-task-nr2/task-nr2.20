const createUserMiddleware = (req, res, next) => {
  console.log("Create User Middleware")
  next();
}
const getUserMiddleware = (req, res, next) => {
  console.log("Get User Middleware")
  next();
}
const getUserByIdMiddleware = (req, res, next) => {
  console.log("Get User by Id Middleware")
  next();
}
const updateUserMiddleware = (req, res, next) => {
  console.log("Update User Middleware")
  next();
}
const deleteUserMiddleware = (req, res, next) => {
  console.log("Delete User Middleware")
  next();
}

module.exports = {
  createUserMiddleware,
  getUserMiddleware,
  getUserByIdMiddleware,
  updateUserMiddleware,
  deleteUserMiddleware,
}