const createDrinkMiddleware = (req, res, next) => {
  console.log("Create Drink Middleware")
  next();
}
const getDrinkMiddleware = (req, res, next) => {
  console.log("Get Drink Middleware")
  next();
}
const getDrinkByIdMiddleware = (req, res, next) => {
  console.log("Get Drink by Id Middleware")
  next();
}
const updateDrinkMiddleware = (req, res, next) => {
  console.log("Update Drink Middleware")
  next();
}
const deleteDrinkMiddleware = (req, res, next) => {
  console.log("Delete Drink Middleware")
  next();
}

module.exports = {
  createDrinkMiddleware,
  getDrinkMiddleware,
  getDrinkByIdMiddleware,
  updateDrinkMiddleware,
  deleteDrinkMiddleware,
}