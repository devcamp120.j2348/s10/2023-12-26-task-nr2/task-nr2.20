const createVoucherMiddleware = (req, res, next) => {
  console.log("Create Voucher Middleware")
  next();
}
const getVoucherMiddleware = (req, res, next) => {
  console.log("Get Voucher Middleware")
  next();
}
const getVoucherByIdMiddleware = (req, res, next) => {
  console.log("Get Voucher by Id Middleware")
  next();
}
const updateVoucherMiddleware = (req, res, next) => {
  console.log("Update Voucher Middleware")
  next();
}
const deleteVoucherMiddleware = (req, res, next) => {
  console.log("Delete Voucher Middleware")
  next();
}

module.exports = {
  createVoucherMiddleware,
  getVoucherMiddleware,
  getVoucherByIdMiddleware,
  updateVoucherMiddleware,
  deleteVoucherMiddleware,
}