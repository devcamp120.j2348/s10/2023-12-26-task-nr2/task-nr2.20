const express = require("express");
const router = express.Router();
const userMiddleware = require("../middlewares/userMiddleware")
const {
  createUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById,
  getAllUsersWithSort
} = require("../controller/userController")

router.get("/user",userMiddleware.getUserMiddleware, getAllUser)
router.get("/limit-users",userMiddleware.getUserMiddleware, getAllUser)
router.get("/skip-users",userMiddleware.getUserMiddleware, getAllUser)
router.get("/skip-limit-users",userMiddleware.getUserMiddleware, getAllUser)
router.get("/sort-skip-limit-users",userMiddleware.getUserMiddleware, getAllUser)
router.get("/sort-users",userMiddleware.getUserMiddleware, getAllUsersWithSort)
router.post("/user",userMiddleware.createUserMiddleware, createUser)
router.get("/user/:id",userMiddleware.getUserByIdMiddleware, getUserById)
router.put("/user/:id",userMiddleware.updateUserMiddleware, updateUserById)
router.delete("/user/:id",userMiddleware.deleteUserMiddleware,deleteUserById)

module.exports = router