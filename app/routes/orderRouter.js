const express = require("express");
const router = express.Router();
const orderMiddleware = require("../middlewares/orderMiddleware")
const {
  createOrder,
  getAllOrder,
  getOrderById,
  updateOrderById,
  deleteOrderById
} = require("../controller/orderController")

router.get("/order",orderMiddleware.getOrderMiddleware, getAllOrder)
router.post("/order",orderMiddleware.createOrderMiddleware, createOrder)
router.get("/order/:id",orderMiddleware.getOrderByIdMiddleware, getOrderById)
router.put("/order/:id",orderMiddleware.updateOrderMiddleware, updateOrderById)
router.delete("/user/:userId/order/:id",orderMiddleware.deleteOrderMiddleware, deleteOrderById)

module.exports = router