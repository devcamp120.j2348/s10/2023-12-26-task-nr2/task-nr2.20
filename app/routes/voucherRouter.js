const express = require("express");
const router = express.Router();
const voucherMiddleware = require("../middlewares/voucherMiddleware")
const {
  createVoucher,
  getAllVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucher
} = require('../controller/voucherController')

router.get("/voucher",voucherMiddleware.getVoucherMiddleware, getAllVoucher)
router.post("/voucher",voucherMiddleware.createVoucherMiddleware,createVoucher)


router.get("/voucher/:id",voucherMiddleware.getVoucherByIdMiddleware, getVoucherById)
router.put("/voucher/:id",voucherMiddleware.updateVoucherMiddleware, updateVoucherById)
router.delete("/order/:orderId/voucher/:id",voucherMiddleware.deleteVoucherMiddleware, deleteVoucher)

module.exports = router