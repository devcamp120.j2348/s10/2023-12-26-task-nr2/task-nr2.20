const express = require("express");
const router = express.Router();
const drinkMiddleware = require("../middlewares/drinkMiddleware")
const {
  getAllDrink,
  createDrink,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById
} = require("../controller/drinkController")

router.get("/drink",drinkMiddleware.getDrinkMiddleware, getAllDrink)


router.post("/drink",drinkMiddleware.createDrinkMiddleware, createDrink)
router.get("/drink/:id",drinkMiddleware.getDrinkByIdMiddleware, getDrinkById)
router.put("/drink/:id",drinkMiddleware.updateDrinkMiddleware, updateDrinkById)
router.delete("/order/:orderId/drink/:id",drinkMiddleware.deleteDrinkMiddleware, deleteDrinkById)

module.exports = router